"""Tests unitaires : bulletins de notes

Utiliser comme:
    pytest tests/unit/test_sco_basic.py

Au besoin, créer un base de test neuve:
    ./tools/create_database.sh SCODOC_TEST

"""

from app.models import FormSemestre, Identite

from config import TestConfig

import app
from app.scodoc import sco_bulletins_json
from app.scodoc import sco_preferences
from tests.unit import sco_fake_gen
from tests.unit import test_sco_basic


DEPT = TestConfig.DEPT_TEST


def test_bulletin_data_classic(test_client):
    """Vérifications sur les bulletins de notes"""
    G = sco_fake_gen.ScoFake(verbose=False)
    app.set_sco_dept(DEPT)
    formsemestre = test_sco_basic.run_sco_basic()
    modimpl = formsemestre.modimpls.first()
    evaluation = modimpl.evaluations.first()
    # S'assure qu'on a bien une formation classique:
    assert formsemestre.formation.is_apc() is False
    etud: Identite = formsemestre.etuds.first()
    assert etud
    # Ici on a un modimpl avec 9 inscrits, 2 evals ayant toutes leurs notes
    # Vérification des min/max évaluation sur le bulletin
    bul = sco_bulletins_json.formsemestre_bulletinetud_published_dict(
        formsemestre.id,
        etud.id,
        force_publishing=True,
        xml_with_decisions=True,
    )
    assert isinstance(bul, dict)
    assert bul["type"] == "classic"
    modules_res = bul["ue"][0]["module"]
    assert len(modules_res) == 1  # 1 seul module complet
    module_res = modules_res[0]
    assert modimpl.module.code == module_res["code"]
    assert len(module_res["evaluation"]) == 2
    note_eval_1 = module_res["evaluation"][0]
    assert "note" in note_eval_1
    assert "min" not in note_eval_1
    assert not sco_preferences.get_preference("bul_show_minmax_eval")
    # Change préférence pour avoir min/max évaluation
    prefs = sco_preferences.get_base_preferences()
    prefs.set(None, "bul_show_minmax_eval", True)
    assert sco_preferences.get_preference("bul_show_minmax_eval")
    # Redemande le bulletin
    bul = sco_bulletins_json.formsemestre_bulletinetud_published_dict(
        formsemestre.id,
        etud.id,
        force_publishing=True,
        xml_with_decisions=True,
    )
    note_eval_1 = bul["ue"][0]["module"][0]["evaluation"][0]
    assert "min" in note_eval_1
    assert "max" in note_eval_1
    min_eval_1 = float(note_eval_1["min"])
    max_eval_1 = float(note_eval_1["max"])
    # la valeur actuelle est 12.34, on s'assure qu'elle n'est pas extrême:
    assert min_eval_1 > 0  # 12.34
    assert max_eval_1 < 20  # 12.34

    # Saisie note pour changer min/max:
    # Met le max à 20:
    G.create_note(evaluation_id=evaluation.id, etudid=etud.id, note=20.0)
    bul = sco_bulletins_json.formsemestre_bulletinetud_published_dict(
        formsemestre.id,
        etud.id,
        force_publishing=True,
        xml_with_decisions=True,
    )
    note_eval_1 = bul["ue"][0]["module"][0]["evaluation"][0]
    assert note_eval_1["max"] == "20.00"
    # Met le min à zero:
    G.create_note(evaluation_id=evaluation.id, etudid=etud.id, note=0.0)
    bul = sco_bulletins_json.formsemestre_bulletinetud_published_dict(
        formsemestre.id,
        etud.id,
        force_publishing=True,
        xml_with_decisions=True,
    )
    note_eval_1 = bul["ue"][0]["module"][0]["evaluation"][0]
    assert note_eval_1["min"] == "00.00"
