#!/usr/bin/env python3
# -*- mode: python -*-
# -*- coding: utf-8 -*-

"""Exemple utilisation API ScoDoc 9 avec jeton obtenu par basic authentication

    Extraction de la liste de tous les modules d'une année scolaire

    Usage:
        cd /opt/scodoc/tests/api
        python -i exemple-api-list-modules.py


Pour utiliser l'API, (sur une base quelconque):
```
cd /opt/scodoc/tests/api

python -i exemple-api-list-modules.py
>>> admin_h = get_auth_headers("admin", "xxx")
>>> GET("/etudiant/etudid/14806", headers=admin_h)
```

Créer éventuellement un fichier `.env` dans /opt/scodoc/tests/api
avec la config du client API:
```
    SCODOC_URL = "http://localhost:5000/"
    API_USER = "admin"
    API_PASSWORD = "test"
```
"""

from pprint import pprint as pp
import requests
import sys
import urllib3
from setup_test_api import (
    API_PASSWORD,  # lus de l'environnement ou du .env
    API_URL,
    API_USER,
    APIError,
    CHECK_CERTIFICATE,
    get_auth_headers,
    GET,
    POST_JSON,
    SCODOC_URL,
)


def logout_api_user():
    r = requests.delete(API_URL + "/tokens", headers=HEADERS, verify=CHECK_CERTIFICATE)
    assert r.status_code == 204


if not CHECK_CERTIFICATE:
    urllib3.disable_warnings()

# Si vous n'utilisez pas .env:
API_USER = "lecteur_api"
API_PASSWORD = "azerty"

HEADERS = get_auth_headers(API_USER, API_PASSWORD)
print("connected to ScoDoc")

# Liste des formsemestres de l'année scolaire
ANNEE_SCOLAIRE = 2023  # int, année de début de l'année scolaire

formsemestres = GET(
    f"/formsemestres/query?annee_scolaire={ANNEE_SCOLAIRE}", headers=HEADERS
)
print(f"Nombre de semestres: {len(formsemestres)}")

r = []  # liste de dict, résultat
for formsemestre in formsemestres:
    print(f"requesting {formsemestre['titre_num']}")
    programme = GET(f"/formsemestre/{formsemestre['id']}/programme", headers=HEADERS)
    for mod_type in ("ressources", "saes", "modules"):
        mods = programme[mod_type]
        for mod in mods:
            r.append(
                {
                    "dept": formsemestre["departement"]["acronym"],
                    "sem_id": formsemestre["id"],
                    "sem_titre": formsemestre["titre"],
                    "sem_modalite": formsemestre["modalite"],
                    "sem_etape": formsemestre["etape_apo"],
                    "mod_type": mod_type[:-1],
                    "mod_code": mod["module"]["code"],
                    "mod_titre": mod["module"]["titre"],
                    "mod_abbrev": mod["module"]["abbrev"],
                    "mod_code_apogee": mod["module"]["code_apogee"],
                    "modimpl_code_apogee": mod["code_apogee"],
                }
            )

# Dump to csv file
SEP = "\t"
with open("/tmp/modules.csv", "w", encoding="utf-8") as f:
    f.write(SEP.join(r[0]) + "\n")
    for l in r:
        # on élimine les éventuels séparateurs des champs...
        f.write(SEP.join([str(x).replace(SEP, " ") for x in l.values()]) + "\n")
