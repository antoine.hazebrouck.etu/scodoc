"""code apo sur modimpls

Revision ID: c8f66652c77f
Revises: 6fb956addd69
Create Date: 2023-11-12 10:01:42.424734

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "c8f66652c77f"
down_revision = "6fb956addd69"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("notes_moduleimpl", schema=None) as batch_op:
        batch_op.add_column(
            sa.Column("code_apogee", sa.String(length=512), nullable=True)
        )
        batch_op.add_column(sa.Column("edt_id", sa.Text(), nullable=True))
        batch_op.create_index(
            batch_op.f("ix_notes_moduleimpl_code_apogee"), ["code_apogee"], unique=False
        )
        batch_op.create_index(
            batch_op.f("ix_notes_moduleimpl_edt_id"), ["edt_id"], unique=False
        )


def downgrade():
    with op.batch_alter_table("notes_moduleimpl", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_notes_moduleimpl_edt_id"))
        batch_op.drop_index(batch_op.f("ix_notes_moduleimpl_code_apogee"))
        batch_op.drop_column("edt_id")
        batch_op.drop_column("code_apogee")
