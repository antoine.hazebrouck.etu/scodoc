"""Evolution ref. Orebut

Revision ID: 197c658cefbb
Revises: 91be8a06d423
Create Date: 2022-01-05 22:25:12.384647

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "197c658cefbb"
down_revision = "91be8a06d423"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column("apc_competence", sa.Column("id_orebut", sa.Text(), nullable=True))
    op.drop_constraint(
        "apc_competence_referentiel_id_titre_key", "apc_competence", type_="unique"
    )
    op.create_index(
        op.f("ix_apc_competence_id_orebut"),
        "apc_competence",
        ["id_orebut"],
        unique=True,
    )
    op.add_column(
        "apc_referentiel_competences", sa.Column("annexe", sa.Text(), nullable=True)
    )
    op.add_column(
        "apc_referentiel_competences",
        sa.Column("type_structure", sa.Text(), nullable=True),
    )
    op.add_column(
        "apc_referentiel_competences",
        sa.Column("type_departement", sa.Text(), nullable=True),
    )
    op.add_column(
        "apc_referentiel_competences",
        sa.Column("version_orebut", sa.Text(), nullable=True),
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column("apc_referentiel_competences", "version_orebut")
    op.drop_column("apc_referentiel_competences", "type_departement")
    op.drop_column("apc_referentiel_competences", "type_structure")
    op.drop_column("apc_referentiel_competences", "annexe")
    op.drop_index(op.f("ix_apc_competence_id_orebut"), table_name="apc_competence")
    op.create_unique_constraint(
        "apc_competence_referentiel_id_titre_key",
        "apc_competence",
        ["referentiel_id", "titre"],
    )
    op.drop_column("apc_competence", "id_orebut")
    # ### end Alembic commands ###
