##############################################################################
# ScoDoc
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
# See LICENSE
##############################################################################

"""Jury édition manuelle des décisions (correction d'erreurs, parcours hors normes)

Non spécifique au BUT.
"""

from flask import render_template
import sqlalchemy as sa

from app.models import (
    ApcValidationAnnee,
    ApcValidationRCUE,
    Identite,
    UniteEns,
    ScolarAutorisationInscription,
    ScolarFormSemestreValidation,
)
from app.views import ScoData


def jury_delete_manual(etud: Identite):
    """Vue (réservée au chef de dept.)
    présentant *toutes* les décisions de jury concernant cet étudiant
    et permettant de les supprimer une à une.
    """
    sem_vals = ScolarFormSemestreValidation.query.filter_by(
        etudid=etud.id, ue_id=None
    ).order_by(ScolarFormSemestreValidation.event_date)
    ue_vals = (
        ScolarFormSemestreValidation.query.filter_by(etudid=etud.id)
        .join(UniteEns)
        .order_by(
            sa.extract("year", ScolarFormSemestreValidation.event_date),
            UniteEns.semestre_idx,
            UniteEns.numero,
            UniteEns.acronyme,
        )
    )
    autorisations = ScolarAutorisationInscription.query.filter_by(
        etudid=etud.id
    ).order_by(
        ScolarAutorisationInscription.semestre_id, ScolarAutorisationInscription.date
    )
    rcue_vals = (
        ApcValidationRCUE.query.filter_by(etudid=etud.id)
        .join(UniteEns, UniteEns.id == ApcValidationRCUE.ue1_id)
        .order_by(UniteEns.semestre_idx, UniteEns.numero, ApcValidationRCUE.date)
    )
    annee_but_vals = ApcValidationAnnee.query.filter_by(etudid=etud.id).order_by(
        ApcValidationAnnee.ordre, ApcValidationAnnee.date
    )
    return render_template(
        "jury/jury_delete_manual.j2",
        etud=etud,
        sem_vals=sem_vals,
        ue_vals=ue_vals,
        autorisations=autorisations,
        rcue_vals=rcue_vals,
        annee_but_vals=annee_but_vals,
        sco=ScoData(),
        title=f"Toutes les décisions de jury enregistrées pour {etud.html_link_fiche()}",
    )
