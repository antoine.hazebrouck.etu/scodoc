##############################################################################
#  Module "Avis de poursuite d'étude"
#  conçu et développé par Cléo Baras (IUT de Grenoble)
##############################################################################

"""Affichages, debug
"""

from flask import g
from app import log

PE_DEBUG = False


# On stocke les logs PE dans g.scodoc_pe_log
# pour ne pas modifier les nombreux appels à pe_print.
def pe_start_log() -> list[str]:
    "Initialize log"
    g.scodoc_pe_log = []
    return g.scodoc_pe_log


def pe_print(*a):
    "Log (or print in PE_DEBUG mode) and store in g"
    lines = getattr(g, "scodoc_pe_log")
    if lines is None:
        lines = pe_start_log()
    msg = " ".join(a)
    lines.append(msg)
    if PE_DEBUG:
        print(msg)
    else:
        log(msg)


def pe_get_log() -> str:
    "Renvoie une chaîne avec tous les messages loggués"
    return "\n".join(getattr(g, "scodoc_pe_log", []))


# Affichage dans le tableur pe en cas d'absence de notes
SANS_NOTE = "-"
NOM_STAT_GROUPE = "statistiques du groupe"
NOM_STAT_PROMO = "statistiques de la promo"
