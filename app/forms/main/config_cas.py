# -*- mode: python -*-
# -*- coding: utf-8 -*-

##############################################################################
#
# ScoDoc
#
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   Emmanuel Viennet      emmanuel.viennet@viennet.net
#
##############################################################################

"""
Formulaire configuration CAS
"""

from flask_wtf import FlaskForm
from wtforms import BooleanField, SubmitField, ValidationError
from wtforms.fields.simple import FileField, StringField
from wtforms.validators import Optional

from app.models import ScoDocSiteConfig


def check_cas_uid_from_mail_regexp(form, field):
    "Vérifie la regexp fournie pour l'extraction du CAS id"
    if not ScoDocSiteConfig.cas_uid_from_mail_regexp_is_valid(field.data):
        raise ValidationError("expression régulière invalide")


def check_cas_edt_id_from_xml_regexp(form, field):
    "Vérifie la regexp fournie pour l'extraction du CAS id"
    if not ScoDocSiteConfig.cas_edt_id_from_xml_regexp_is_valid(field.data):
        raise ValidationError("expression régulière pour edt_id invalide")


class ConfigCASForm(FlaskForm):
    "Formulaire paramétrage CAS"
    cas_enable = BooleanField("Activer le CAS")
    cas_force = BooleanField(
        "Forcer l'utilisation de CAS (tous les utilisateurs seront redirigés vers le CAS)"
    )
    cas_allow_for_new_users = BooleanField(
        "Par défaut, autoriser le CAS aux nouveaux utilisateurs"
    )

    cas_server = StringField(
        label="URL du serveur CAS",
        description="""url complète. Commence en général par <tt>https://</tt>.""",
    )
    cas_login_route = StringField(
        label="Optionnel: route du login CAS",
        description="""ajouté à l'URL du serveur: exemple <tt>/cas</tt>
            (si commence par <tt>/</tt>, part de la racine)""",
        default="/cas",
    )
    cas_logout_route = StringField(
        label="Optionnel: route du logout CAS",
        description="""ajouté à l'URL du serveur: exemple <tt>/cas/logout</tt>""",
        default="/cas/logout",
    )
    cas_validate_route = StringField(
        label="Optionnel: route de validation CAS",
        description="""ajouté à l'URL du serveur: exemple <tt>/cas/serviceValidate</tt>""",
        default="/cas/serviceValidate",
    )

    cas_attribute_id = StringField(
        label="Attribut CAS utilisé comme id (laissez vide pour prendre l'id par défaut)",
        description="""Le champ CAS qui sera considéré comme l'id unique des
        comptes utilisateurs.""",
    )

    cas_uid_from_mail_regexp = StringField(
        label="Optionnel: expression pour extraire l'identifiant utilisateur",
        description="""regexp python appliquée au mail institutionnel de l'utilisateur,
        dont le premier groupe doit donner l'identifiant CAS.
        Si non fournie, le super-admin devra saisir cet identifiant pour chaque compte.
        Par exemple, <tt>(.*)@</tt> indique que le mail sans le domaine (donc toute
        la partie avant le <tt>@</tt>) est l'identifiant.
        Pour prendre le mail complet, utiliser <tt>(.*)</tt>.
        """,
        validators=[Optional(), check_cas_uid_from_mail_regexp],
    )

    cas_edt_id_from_xml_regexp = StringField(
        label="Optionnel: expression pour extraire l'identifiant edt",
        description="""regexp python appliquée à la réponse XML du serveur CAS pour
        retrouver l'id de l'utilisateur sur le SI de l'institution, et notamment sur les
        calendrier d'emploi du temps. Par exemple, si cet id est renvoyé dans le champ
        <b>supannEmpId</b>, utiliser:
        <tt>&lt;cas:supannEmpId&gt;(.*?)&lt;/cas:supannEmpId&gt;</tt>
        """,
        validators=[Optional(), check_cas_edt_id_from_xml_regexp],
    )

    cas_ssl_verify = BooleanField("Vérification du certificat SSL")
    cas_ssl_certificate_file = FileField(
        label="Certificat (PEM)",
        description="""Le contenu du certificat PEM
        (commence typiquement par <tt>-----BEGIN CERTIFICATE-----</tt>)""",
    )

    submit = SubmitField("Valider")
    cancel = SubmitField("Annuler", render_kw={"formnovalidate": True})
