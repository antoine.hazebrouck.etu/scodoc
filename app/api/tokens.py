from flask_json import as_json
from app import db, log
from app.api import api_bp as bp
from app.auth.logic import basic_auth, token_auth


@bp.route("/tokens", methods=["POST"])
@basic_auth.login_required
@as_json
def get_token():
    "renvoie un jeton jwt pour l'utilisateur courant"
    token = basic_auth.current_user().get_token()
    log(f"API: giving token to {basic_auth.current_user()}")
    db.session.commit()
    return {"token": token}


@bp.route("/tokens", methods=["DELETE"])
@token_auth.login_required
def revoke_token():
    "révoque le jeton de l'utilisateur courant"
    user = token_auth.current_user()
    user.revoke_token()
    db.session.commit()
    log(f"API: revoking token for {user}")
    return "", 204
