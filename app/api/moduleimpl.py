##############################################################################
# ScoDoc
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
# See LICENSE
##############################################################################

"""
  ScoDoc 9 API : accès aux moduleimpl
"""

from flask_json import as_json
from flask_login import login_required

import app
from app.api import api_bp as bp, api_web_bp
from app.decorators import scodoc, permission_required
from app.models import ModuleImpl
from app.scodoc import sco_liste_notes
from app.scodoc.sco_permissions import Permission


@bp.route("/moduleimpl/<int:moduleimpl_id>")
@api_web_bp.route("/moduleimpl/<int:moduleimpl_id>")
@login_required
@scodoc
@permission_required(Permission.ScoView)
@as_json
def moduleimpl(moduleimpl_id: int):
    """
    Retourne un moduleimpl en fonction de son id

    moduleimpl_id : l'id d'un moduleimpl

    Exemple de résultat :
        {
          "id": 1,
          "formsemestre_id": 1,
          "module_id": 1,
          "responsable_id": 2,
          "moduleimpl_id": 1,
          "ens": [],
          "module": {
            "heures_tp": 0,
            "code_apogee": "",
            "titre": "Initiation aux réseaux informatiques",
            "coefficient": 1,
            "module_type": 2,
            "id": 1,
            "ects": null,
            "abbrev": "Init aux réseaux informatiques",
            "ue_id": 1,
            "code": "R101",
            "formation_id": 1,
            "heures_cours": 0,
            "matiere_id": 1,
            "heures_td": 0,
            "semestre_id": 1,
            "numero": 10,
            "module_id": 1
          }
        }
    """
    modimpl = ModuleImpl.get_modimpl(moduleimpl_id)
    return modimpl.to_dict(convert_objects=True)


@bp.route("/moduleimpl/<int:moduleimpl_id>/inscriptions")
@api_web_bp.route("/moduleimpl/<int:moduleimpl_id>/inscriptions")
@login_required
@scodoc
@permission_required(Permission.ScoView)
@as_json
def moduleimpl_inscriptions(moduleimpl_id: int):
    """Liste des inscriptions à ce moduleimpl
    Exemple de résultat :
        [
          {
            "id": 1,
            "etudid": 666,
            "moduleimpl_id": 1234,
          },
          ...
        ]
    """
    modimpl = ModuleImpl.get_modimpl(moduleimpl_id)
    return [i.to_dict() for i in modimpl.inscriptions]


@bp.route("/moduleimpl/<int:moduleimpl_id>/notes")
@api_web_bp.route("/moduleimpl/<int:moduleimpl_id>/notes")
@login_required
@scodoc
@permission_required(Permission.ScoView)
def moduleimpl_notes(moduleimpl_id: int):
    """Liste des notes dans ce moduleimpl
    Exemple de résultat :
        [
          {
            "etudid": 17776, // code de l'étudiant
            "nom": "DUPONT",
            "prenom": "Luz",
            "38411": 16.0, // Note dans l'évaluation d'id 38411
            "38410": 15.0,
            "moymod": 15.5, // Moyenne INDICATIVE module
            "moy_ue_2875": 15.5, // Moyenne vers l'UE 2875
            "moy_ue_2876": 15.5, // Moyenne vers l'UE 2876
            "moy_ue_2877": 15.5  // Moyenne vers l'UE 2877
          },
          ...
        ]
    """
    modimpl = ModuleImpl.get_modimpl(moduleimpl_id)
    app.set_sco_dept(modimpl.formsemestre.departement.acronym)
    table, _ = sco_liste_notes.do_evaluation_listenotes(
        moduleimpl_id=modimpl.id, fmt="json"
    )
    return table
