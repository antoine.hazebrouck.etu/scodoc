// Formulaire formsemestre_createwithmodules

function change_semestre_id() {
  var semestre_id = $("#tf_semestre_id")[0].value;
  for (var i = -1; i < 12; i++) {
    $(".sem" + i).hide();
  }
  $(".sem" + semestre_id).show();
}

$(window).on("load", function () {
  change_semestre_id();
});
