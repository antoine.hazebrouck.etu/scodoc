Bienvenue {{ user.prenom }} {{ user.nom }},

Votre accès à ScoDoc vient d'être validé.
Votre identifiant ScoDoc est: {{ user.user_name }}

{% if cas_force %}
<p>
    Pour vous connecter, vous devrez utiliser votre identifiant universitaire
    sur le système d'authentification de votre établissement (CAS, ENT).
</p>
{% endif %}

{% if token %}
    Pour initialiser votre mot de passe ScoDoc, suivre le lien:
    {{ url_for('auth.reset_password', token=token, _external=True) }}
{% endif %}

A bientôt !

Ce message a été généré automatiquement par le serveur ScoDoc.