# -*- mode: python -*-
# -*- coding: utf-8 -*-

"""Functions checking permissions for some common operations
"""
from flask import g
from flask_login import current_user

from app import db
from app.auth.models import User
import app.scodoc.notesdb as ndb
from app.scodoc.sco_permissions import Permission
from app.scodoc import html_sco_header
from app.scodoc import sco_etud
from app.scodoc import sco_exceptions


def can_suppress_annotation(annotation_id):
    """True if current user can suppress this annotation
    Seuls l'auteur de l'annotation et le chef de dept peuvent supprimer
    une annotation.
    """
    cnx = ndb.GetDBConnexion()
    annos = sco_etud.etud_annotations_list(cnx, args={"id": annotation_id})
    if len(annos) != 1:
        raise sco_exceptions.ScoValueError("annotation inexistante !")
    anno = annos[0]
    return (current_user.user_name == anno["author"]) or current_user.has_permission(
        Permission.EtudAddAnnotations
    )


def can_edit_suivi():
    """Vrai si l'utilisateur peut modifier les informations de suivi sur la page etud" """
    return current_user.has_permission(Permission.EtudChangeAdr)


def is_chef_or_diretud(sem):  # remplacé par formsemestre.est_chef_or_diretud
    "Vrai si utilisateur est admin, chef dept ou responsable du semestre"
    if (
        current_user.has_permission(Permission.EditFormSemestre)
        or current_user.id in sem["responsables"]
    ):
        return True
    return False


def check_access_diretud(
    formsemestre_id, required_permission=Permission.EditFormSemestre
):
    """Check if access granted: responsable or EditFormSemestre
    Return True|False, HTML_error_page
    """
    from app.scodoc import sco_formsemestre

    sem = sco_formsemestre.get_formsemestre(formsemestre_id)
    header = html_sco_header.sco_header(page_title="Accès interdit")
    footer = html_sco_header.sco_footer()
    if (current_user.id not in sem["responsables"]) and not current_user.has_permission(
        required_permission
    ):
        return (
            False,
            "\n".join(
                [
                    header,
                    "<h2>Opération non autorisée pour %s</h2>" % current_user,
                    "<p>Responsable de ce semestre : <b>%s</b></p>"
                    % ", ".join(
                        [
                            db.session.get(User, i).get_prenomnom()
                            for i in sem["responsables"]
                        ]
                    ),
                    footer,
                ]
            ),
        )
    else:
        return True, ""


def can_handle_passwd(user: User, allow_admindepts=False) -> bool:
    """True if the current user can see or change passwd info of user.
    If allow_admindepts, allow Admin from all depts (so they can view users from other depts
    and add roles to them).
    user is a User instance.
    """
    if not user:
        return False
    if current_user.is_administrator():
        return True  # super admin
    # Anyone can change his own passwd (or see his informations)
    if user.user_name == current_user.user_name:
        return True
    # If don't have permission in the current dept, abort
    if not current_user.has_permission(Permission.UsersAdmin, g.scodoc_dept):
        return False
    # Now check that current_user can manage users from this departement
    if not current_user.dept:
        return True  # if no dept, can access users from all depts !
    if (current_user.dept == user.dept) or allow_admindepts:
        return True
    return False
