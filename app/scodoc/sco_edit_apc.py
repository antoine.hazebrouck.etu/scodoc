##############################################################################
#
# ScoDoc
#
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   Emmanuel Viennet      emmanuel.viennet@viennet.net
#
##############################################################################

"""Édition formation APC (BUT)
"""

from flask.templating import render_template

from app import db
from app.but import apc_edit_ue
from app.models import UniteEns, Matiere, Module, FormSemestre, ModuleImpl
from app.models.validations import ScolarFormSemestreValidation
from app.scodoc import codes_cursus
import app.scodoc.sco_utils as scu
from app.scodoc import sco_groups
from app.scodoc.sco_utils import ModuleType


def html_edit_formation_apc(
    formation,
    semestre_idx=None,
    editable=True,
    tag_editable=True,
):
    """Formulaire html pour visualisation ou édition d'une formation APC.
    - Les UEs
    - Les ressources
    - Les SAÉs
    """
    cursus = formation.get_cursus()
    assert cursus.APC_SAE

    ressources = formation.modules.filter_by(module_type=ModuleType.RESSOURCE).order_by(
        Module.semestre_id, Module.numero, Module.code
    )
    saes = formation.modules.filter_by(module_type=ModuleType.SAE).order_by(
        Module.semestre_id, Module.numero, Module.code
    )
    if semestre_idx is None:
        semestre_ids = range(1, cursus.NB_SEM + 1)
    else:
        semestre_ids = [semestre_idx]
    other_modules = formation.modules.filter(
        Module.module_type.is_distinct_from(ModuleType.SAE),
        Module.module_type.is_distinct_from(ModuleType.RESSOURCE),
    ).order_by(
        Module.semestre_id, Module.module_type.desc(), Module.numero, Module.code
    )

    ues_by_sem = {}
    ects_by_sem = {}
    for semestre_idx in semestre_ids:
        ues_by_sem[semestre_idx] = formation.ues.filter_by(
            semestre_idx=semestre_idx
        ).order_by(UniteEns.semestre_idx, UniteEns.numero, UniteEns.acronyme)
        ects = [
            ue.ects
            for ue in ues_by_sem[semestre_idx]
            if ue.type != codes_cursus.UE_SPORT
        ]
        if None in ects:
            ects_by_sem[semestre_idx] = '<span class="missing_ue_ects">manquant</span>'
        else:
            ects_by_sem[semestre_idx] = f"{sum(ects):g}"

    arrow_up, arrow_down, arrow_none = sco_groups.get_arrow_icons_tags()

    icons = {
        "arrow_up": arrow_up,
        "arrow_down": arrow_down,
        "arrow_none": arrow_none,
        "delete": scu.icontag(
            "delete_small_img",
            title="Supprimer (module inutilisé)",
            alt="supprimer",
        ),
        "delete_disabled": scu.icontag(
            "delete_small_dis_img",
            title="Suppression impossible (utilisé dans des semestres)",
        ),
    }

    H = [
        render_template(
            "pn/form_ues.j2",
            formation=formation,
            semestre_ids=semestre_ids,
            editable=editable,
            tag_editable=tag_editable,
            icons=icons,
            ues_by_sem=ues_by_sem,
            ects_by_sem=ects_by_sem,
            scu=scu,
            codes_cursus=codes_cursus,
        ),
    ]
    for semestre_idx in semestre_ids:
        ressources_in_sem = ressources.filter_by(semestre_id=semestre_idx)
        saes_in_sem = saes.filter_by(semestre_id=semestre_idx)
        other_modules_in_sem = other_modules.filter_by(semestre_id=semestre_idx)
        matiere_parent = Matiere.query.filter(
            Matiere.ue_id == UniteEns.id,
            UniteEns.formation_id == formation.id,
            UniteEns.semestre_idx == semestre_idx,
            UniteEns.type != codes_cursus.UE_SPORT,
        ).first()
        H += [
            (
                render_template(
                    "pn/form_mods.j2",
                    formation=formation,
                    titre=f"Ressources du S{semestre_idx}",
                    create_element_msg="créer une nouvelle ressource",
                    # matiere_parent=matiere_parent,
                    modules=ressources_in_sem,
                    module_type=ModuleType.RESSOURCE,
                    editable=editable,
                    tag_editable=tag_editable,
                    icons=icons,
                    scu=scu,
                    semestre_id=semestre_idx,
                )
                if ues_by_sem[semestre_idx].count() > 0
                else ""
            ),
            (
                render_template(
                    "pn/form_mods.j2",
                    formation=formation,
                    titre=f"Situations d'Apprentissage et d'Évaluation (SAÉs) S{semestre_idx}",
                    create_element_msg="créer une nouvelle SAÉ",
                    # matiere_parent=matiere_parent,
                    modules=saes_in_sem,
                    module_type=ModuleType.SAE,
                    editable=editable,
                    tag_editable=tag_editable,
                    icons=icons,
                    scu=scu,
                    semestre_id=semestre_idx,
                )
                if ues_by_sem[semestre_idx].count() > 0
                else ""
            ),
            (
                render_template(
                    "pn/form_mods.j2",
                    formation=formation,
                    titre=f"Autres modules (non BUT) du S{semestre_idx}",
                    create_element_msg="créer un nouveau module",
                    modules=other_modules_in_sem,
                    module_type=ModuleType.STANDARD,
                    editable=editable,
                    tag_editable=tag_editable,
                    icons=icons,
                    scu=scu,
                    semestre_id=semestre_idx,
                )
                if ues_by_sem[semestre_idx].count() > 0
                else """<span class="fontred">créer une UE pour pouvoir ajouter des modules</span>"""
            ),
        ]

    return "\n".join(H)


def html_ue_infos(ue):
    """Page d'information sur une UE"""
    from app.views import ScoData

    formsemestres = (
        db.session.query(FormSemestre)
        .filter(
            ue.id == Module.ue_id,
            Module.id == ModuleImpl.module_id,
            FormSemestre.id == ModuleImpl.formsemestre_id,
        )
        .all()
    )
    nb_etuds_valid_ue = ScolarFormSemestreValidation.query.filter_by(
        ue_id=ue.id
    ).count()
    can_safely_be_suppressed = (
        (nb_etuds_valid_ue == 0)
        and (len(formsemestres) == 0)
        and ue.modules.count() == 0
        and ue.matieres.count() == 0
    )
    return render_template(
        "pn/ue_infos.j2",
        titre=f"UE {ue.acronyme} {ue.titre or ''}",
        ue=ue,
        formsemestres=formsemestres,
        nb_etuds_valid_ue=nb_etuds_valid_ue,
        can_safely_be_suppressed=can_safely_be_suppressed,
        sco=ScoData(),
    )
